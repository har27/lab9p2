/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetable;

/**
 *
 * @author hptop
 */
public class Carrot extends Vegetable {
   public Carrot (String colour, int size, String name){
       super(colour, size, name);
   }
@Override
   public boolean isRipe(){
       return false;
   }
}
