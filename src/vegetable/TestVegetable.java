/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetable;

import java.util.ArrayList;

/**
 *
 * @author hptop
 */
public class TestVegetable {

    public static void main(String args[]) {

        VegetableFactory aVegetableFactory = VegetableFactory.createInstance();
        aVegetableFactory.createVegetable("pink", 5, "Beet",
                VegetableType.BEET);
        aVegetableFactory.createVegetable("red", 3, "Carrot",
                VegetableType.CARROT);
        ArrayList<Vegetable>veggies = (ArrayList<Vegetable>) aVegetableFactory.getVeggies();
        for (Vegetable veggie : veggies) {
            System.out.print(veggie.getName() + "is ");
            if (!veggie.isRipe()) {
                System.out.print("not ");
            }
            System.out.println("Ripe");
        }
    }
}
