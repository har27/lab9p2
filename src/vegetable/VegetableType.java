/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package vegetable;

/**
 *
 * @author harpreet singh
 */

 

// enum for vegetable type
public enum VegetableType {

 

   // list down types
   // for beets and
   // carrots
   BEET,
   CARROT
}
