/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetable;

/**
 *
 * @author hptop
 */
public abstract class Vegetable {

    private String colour;
    private int size;
    private String name;

    Vegetable(String colour, int size, String name) {
        this.colour = colour;
        this.size = size;
        this.name = name;
    }

    abstract boolean isRipe();

    public String getColour() {
        return this.colour;
    }

    public int getSize() {
        return this.size;
    }

    public String getName() {
        return this.name;
    }
}
